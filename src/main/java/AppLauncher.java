import ch.ctsoft.divisor.exceptions.RunnerMissconfigurationException;
import ch.ctsoft.divisor.runner.Runner;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public class AppLauncher {	
	public static void main(String[] args) throws RunnerMissconfigurationException {
		Runner runner = new Runner();
		try {
			runner.checkConfiguration();
		} catch (RunnerMissconfigurationException rme){
			System.out.println(rme.getMessage());
			System.exit(0);
		}
		runner.execute();
	}
}
