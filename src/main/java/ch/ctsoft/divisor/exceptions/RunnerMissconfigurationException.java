package ch.ctsoft.divisor.exceptions;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public class RunnerMissconfigurationException extends Exception{
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RunnerMissconfigurationException(String message) { super(message); }
}
