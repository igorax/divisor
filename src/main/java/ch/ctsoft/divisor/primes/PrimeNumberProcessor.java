package ch.ctsoft.divisor.primes;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public class PrimeNumberProcessor implements IPrimeNumberPresentation{
	
	private NumberWrapper numberWrapper;
	private int threadId;
	
	public PrimeNumberProcessor(NumberWrapper numberWrapper, int threadId) {
		this.numberWrapper = numberWrapper;
		this.threadId = threadId;
	}
		
	public int getThreadId() {
		return threadId;
	}
	
	public int getNumberWrapperValue() {
		return numberWrapper.getValue();
	}
	
	@Override
	public NumberWrapper getNumberWrapper() {
		return numberWrapper;
	}
	
	@Override
	public String getPresentationString() {
		return numberWrapper.getValue() + " by Thread " + threadId;
	}
}
