package ch.ctsoft.divisor.primes;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public interface INumber {
	boolean isPrimeNumber();
}
