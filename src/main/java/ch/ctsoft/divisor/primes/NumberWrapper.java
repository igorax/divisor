package ch.ctsoft.divisor.primes;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public class NumberWrapper implements INumber{
	private int value;
	
	public NumberWrapper(int value) {
		this.value = value;
	}

	@Override
	public boolean isPrimeNumber() {
		//check if n is a multiple of 2
	    if (value%2==0 && value > 2 || value < 2) return false;
	    //if not, then just check the odds
	    for(int i=3;i*i<=value;i+=2) {
	        if(value%i==0)
	            return false;
	    }
	    return true;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
