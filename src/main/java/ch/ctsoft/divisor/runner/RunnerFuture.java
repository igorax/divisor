package ch.ctsoft.divisor.runner;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public class RunnerFuture implements Future<RunnerThread>{

	private Future<RunnerThread> future;
	
	public RunnerFuture(Future<RunnerThread> future) {
		this.setFuture(future);
	}
	
	@Override
	public boolean cancel(boolean arg0) {
		return future.cancel(arg0);
	}

	@Override
	public RunnerThread get() throws InterruptedException, ExecutionException {
		return future.get();
	}

	@Override
	public RunnerThread get(long arg0, TimeUnit arg1) throws InterruptedException, ExecutionException, TimeoutException {
		return future.get(arg0, arg1);
	}

	@Override
	public boolean isCancelled() {
		return future.isCancelled();
	}

	@Override
	public boolean isDone() {
		return future.isDone();
	}

	public Future<RunnerThread> getFuture() {
		return future;
	}

	public void setFuture(Future<RunnerThread> future) {
		this.future = future;
	}
}
