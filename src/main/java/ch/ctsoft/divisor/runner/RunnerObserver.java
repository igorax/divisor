package ch.ctsoft.divisor.runner;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public class RunnerObserver{
	private RunnerFuture future;
	
	public RunnerObserver(RunnerFuture future) {
		this.future = future;
	}

	public RunnerFuture getFuture() {
		return future;
	}	
}
