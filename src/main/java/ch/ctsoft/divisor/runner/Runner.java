package ch.ctsoft.divisor.runner;

import java.util.ArrayList;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import ch.ctsoft.divisor.exceptions.RunnerMissconfigurationException;
import ch.ctsoft.divisor.primes.PrimeNumberProcessor;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public class Runner{
	
	private int threadCount = 10;
	private int intervalFrom = 0;
	private int intervalTo = 100;

	private Collection<RunnerThread> runnerThreads = new ArrayList<>();
	private Collection<RunnerObserver> runnerThreadObservers = new ArrayList<>();
	private Set<PrimeNumberProcessor> primeNumbers;
	
	public Runner(int threadCount, int intervalFrom, int intervalTo) {
		super();
		this.threadCount = threadCount;
		this.intervalFrom = intervalFrom;
		this.intervalTo = intervalTo;
	}
	
	public Runner() {
		super();
	}
	
	public void execute(){	
		instanceSet();
		ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
		int interval = (intervalTo - intervalFrom);
		int steps = (interval / threadCount);
		int remaining = (interval % threadCount);
		
		createRunnerThreads(steps, remaining);
		
		Consumer<RunnerThread> runnerThreadConsumer = runnerThread -> {
			Future<?> future = executorService.submit(runnerThread);
			
			@SuppressWarnings("unchecked")
			RunnerFuture runnerFuture = new RunnerFuture((Future<RunnerThread>) future);
			runnerThreadObservers.add(new RunnerObserver(runnerFuture));
		};
		
		runnerThreads.forEach(runnerThreadConsumer);
		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES); //wait for completion of every task
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
		
		for(PrimeNumberProcessor pnp : primeNumbers) {
			System.out.println(pnp.getPresentationString());
		}
	}
	
	public void createRunnerThreads(int steps, int remaining) {
		
		int from = intervalFrom - remaining;
		int to = (from + steps);
		
		for (int i = 1; i <= threadCount; i++) {
			runnerThreads.add(new RunnerThread(from, to, this, i));
			from = to + 1;
			to = to + steps;
		}
		
	}
	
	public void checkConfiguration() throws RunnerMissconfigurationException{
		if(intervalFrom >= intervalTo) {
			throw new RunnerMissconfigurationException("Please specify a valid interval");
		}
		if ((intervalTo - intervalFrom) < threadCount) {
			throw new RunnerMissconfigurationException("Please specify a thread count greater than the interval");
		}
		
	}
	
	public void add(PrimeNumberProcessor pnp) {
		primeNumbers.add(pnp);
	}
	
	public void instanceSet() {
		primeNumbers = Collections.synchronizedSet(new TreeSet<>(new Comparator<PrimeNumberProcessor>() {			
			@Override
			public int compare(PrimeNumberProcessor pnp1, PrimeNumberProcessor pnp2) {
				if(pnp1.getNumberWrapperValue() < pnp2.getNumberWrapperValue()) {
					return 0;
				}
				return 1;
			}
	    }));
	}
	
	public Collection<RunnerObserver> getRunnerThreadObservers(){
		return runnerThreadObservers;
	}
	
	public Collection<RunnerThread> getRunnerThreads(){
		return runnerThreads;
	}

	public int getIntervalFrom() {
		return intervalFrom;
	}

	public int getIntervalTo() {
		return intervalTo;
	}

	public int getThreadCount() {
		return threadCount;
	}
}
