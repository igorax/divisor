package ch.ctsoft.divisor.runner;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public abstract class ThreadBase {
	private final int id;
	
	public ThreadBase(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
