package ch.ctsoft.divisor.runner;

import ch.ctsoft.divisor.primes.NumberWrapper;
import ch.ctsoft.divisor.primes.PrimeNumberProcessor;

/**
 * 
 * @author Christian Egli
 * @version 1.0
 *
 */
public class RunnerThread extends ThreadBase implements Runnable{

	private int from;
	private int to;
	private Runner runner;
	
	public RunnerThread(int from, int to, Runner runner, int id) {
		super(id);
		this.from = from;
		this.to = to;
		this.runner = runner;
	}

	@Override
	public void run() {
		int n = from;
		while (n <= to) {
			NumberWrapper number = new NumberWrapper(n);
			if(number.isPrimeNumber()) {
				PrimeNumberProcessor pnp = new PrimeNumberProcessor(number, getId());
				runner.add(pnp);
			}
			n++;
		}
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}
}
