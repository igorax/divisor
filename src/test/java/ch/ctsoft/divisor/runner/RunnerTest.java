package ch.ctsoft.divisor.runner;

import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Test;

public class RunnerTest {
	
	@Test
	public void testRunnerBehaviour(){
		Runner runner = new Runner(2,10,100);
		runner.execute();
				
		Assert.assertEquals(runner.getRunnerThreads().size(), runner.getThreadCount());
		
		Collection<RunnerObserver> observers = runner.getRunnerThreadObservers();
		
		Consumer<RunnerObserver> runnerObserverConsumerIsDone = observer -> {
			if(!observer.getFuture().isDone()) {
				fail(); //fail if a job is not done
			}			
		};
		
		Consumer<RunnerObserver> runnerObserverConsumerIsCancelled = observer -> {
			if(observer.getFuture().isCancelled()) {
				fail(); //fail if a job is cancelled
			}			
		};

		observers.forEach(runnerObserverConsumerIsDone);
		observers.forEach(runnerObserverConsumerIsCancelled);
		
	}
}
