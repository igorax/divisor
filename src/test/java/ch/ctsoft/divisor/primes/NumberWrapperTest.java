package ch.ctsoft.divisor.primes;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

public class NumberWrapperTest {
	
	@Test
	public void testPrimeNumberAlgorithm() {
		List<Integer> primeList = new ArrayList<>();
		
		//generate prime number list from 0 to 17
		for(int i = 0; i <= 17; i++) {
			NumberWrapper numberWrapper = new NumberWrapper(i);
			if(numberWrapper.isPrimeNumber()) {
				primeList.add(numberWrapper.getValue());
			}
		}
		
		//check if the algorithm works
		Assert.assertEquals(primeList.contains(0), false);
		Assert.assertEquals(primeList.contains(1), false);
		Assert.assertEquals(primeList.contains(2), true);
		Assert.assertEquals(primeList.contains(3), true);
		Assert.assertEquals(primeList.contains(4), false);
		Assert.assertEquals(primeList.contains(5), true);
		Assert.assertEquals(primeList.contains(6), false);
		Assert.assertEquals(primeList.contains(7), true);
		Assert.assertEquals(primeList.contains(8), false);
		Assert.assertEquals(primeList.contains(9), false);
		Assert.assertEquals(primeList.contains(10), false);
		Assert.assertEquals(primeList.contains(11), true);
		Assert.assertEquals(primeList.contains(12), false);
		Assert.assertEquals(primeList.contains(13), true);
		Assert.assertEquals(primeList.contains(14), false);
		Assert.assertEquals(primeList.contains(15), false);
		Assert.assertEquals(primeList.contains(16), false);
		Assert.assertEquals(primeList.contains(17), true);
	}

}
